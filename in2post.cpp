

#include <iostream>
#include <string>
#include "stack.h"
#include <fstream>
#include <cstring>

char isOperator(std::string);
char isOperator(char);

int main(int argc, const char * argv[]) {
    
    
    cop4530::Stack<std::string> stringStack;
    cop4530::Stack<std::string> opStack;
    std::vector<std::string> ifArray;
    std::vector<std::string> pfArray;
    cop4530::Stack<double> resultStack;
    
    char buffer[1000];
    std::string last;
    
    
    while (true){
        
        bool varFound = false;
        
        std::cout << "Enter an infix expression (\"exit\" to quit): ";
        std::cin.getline(buffer, 1000);
        
        if (!strcmp(buffer, "exit") || std::cin.eof()){
            return 0;
        }
        
        
        std::cout << "\nPostfix expression: ";
        
        std::string holder;
        
        for (int i = 0; buffer[i] != 0; i++){
            
            if (buffer[i] == ' '){
                ifArray.push_back(holder);
                holder = "";
                continue;
            }
            else{
                if (!isdigit(buffer[i]) && !isOperator(buffer[i]) && buffer[i] != ')'){
                    varFound = true;
                }
                    
                holder.push_back(buffer[i]);
                if (buffer[i + 1] == 0){
                    ifArray.push_back(holder);
                }
            }
        }
       
     //   return 0;

        for (int i = 0; i <= ifArray.size(); i++){
            if (i  == ifArray.size()){
                if (isOperator(last)){
                    std::cout << "\nTrailing operator error. Quitting...\n";
                    return -36;
                }
                while (!opStack.empty()){
                
                    
                    if (opStack.top() == "(") {
                        std::cout << "\nParenthetical error1.  Quitting...\n";
                        return -36;
                    }
                
                    
                    std::cout << opStack.top() << " ";
                    pfArray.push_back(opStack.top());
                    opStack.pop();
                }
                break;
            }
        
            else if (isOperator(ifArray[i])){
                while (!opStack.empty()){
                    if((ifArray[i] == "*" || ifArray[i] == "/")&&(opStack.top() == "+" || opStack.top() == "-"))
                        break;
                    else if (opStack.top() == "("){
                        break;
                    }
                    else if (ifArray[i] == "("){
                        break;
                    }
                    else{
                    pfArray.push_back(opStack.top());
                    std::cout << opStack.top() << " ";
                    opStack.pop();
                    }
                }
                opStack.push(ifArray[i]);
                
            }
            else if (ifArray[i] == ")"){
                if(isOperator(last) && last != "("){
                    std::cout << "\nParenthetical error. Quitting...\n";
                    return -36;
                }
                while (!(opStack.top() == "(")) {
                    pfArray.push_back(opStack.top());
                    std::cout << opStack.top() << " ";
                    opStack.pop();
                    if (opStack.empty()){
                        std::cout << "\nParenthetical error. Quitting...\n";
                        return -36;
                    }
                    
                
                    if (opStack.top() == "("){
                        opStack.pop();
                        break;
                    }
                }
                
            
            }
            else{
                pfArray.push_back(ifArray[i]);
                std::cout << ifArray[i] << " ";
            }
            last = ifArray[i];
        }
    
    
        std::cout << "\nPostfix evaluation: ";
        
        for (int i = 0; i < pfArray.size(); i++) {
            std::cout << pfArray[i] << " ";
        }
        std::cout << "= ";
    
    
        if (!varFound){                                                                                    //calc result
            for (int i = 0; i < pfArray.size(); i++) {
        
                if(!isOperator(pfArray[i])){
                    try {
                    resultStack.push(std::stod(pfArray[i]));
                    } catch (std::invalid_argument) {
                    std::cout << "invalid argument at " << i ;
                    }
                }
                else{
                    double op1, op2;
                    switch (isOperator(pfArray[i])) {
                        case '+':
                            op2 = resultStack.top();
                            resultStack.pop();
                            op1 = resultStack.top();
                            resultStack.pop();
                            
                            resultStack.push(op1 + op2);
                            break;
                        case '-':
                            op2 = resultStack.top();
                            resultStack.pop();
                            op1 = resultStack.top();
                            resultStack.pop();
                    
                            resultStack.push(op1 - op2);
                            break;
                        case '*':
                            op2 = resultStack.top();
                            resultStack.pop();
                            op1 = resultStack.top();
                            resultStack.pop();
                            
                            resultStack.push(op1 * op2);
                            break;
                        case '/':
                            op2 = resultStack.top();
                            resultStack.pop();
                            op1 = resultStack.top();
                            resultStack.pop();
                    
                            resultStack.push(op1 / op2);
                            break;
                    
                        default:
                            break;
                    }
                }
            }
            std::cout << resultStack.top() << "\n";
        }
        else{
            for (int i = 0; i < pfArray.size(); i++) {
                std::cout << pfArray[i] << " ";
            }
            std::cout << "\n";
        }
    
        resultStack.clear();
        pfArray.clear();
        ifArray.clear();
        
    }
    
    return 0;
}



char isOperator(std::string s){                                                        //check for operators
    if ((s == "*") || (s == "+") || (s == "/") || (s == "-") || (s == "("))            //and open parenthesis
        return s[0];
    else
        return 0;
    
}

char isOperator(char s){                                                        //check for operators
    if ((s == '*') || (s == '+') || (s == '/') || (s == '-') || (s == '('))            //and open parenthesis
        return s;
    else
        return 0;
    
}
