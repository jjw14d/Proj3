

#ifndef Stack_h
#define Stack_h
#include <vector>
#include <algorithm>

namespace cop4530 {
    
    template <typename T>
    class Stack{
    
    public:
        Stack();                                            //default constructor
        ~Stack();                                           //destructor
        Stack(const Stack<T>&);                             //copy constructor
        Stack(Stack<T> &&);                                 //move constructor
        Stack<T>& operator= (const Stack <T>&);             //copy assignment operator
        Stack<T> & operator=(Stack<T> &&);                  //move assignment operator
        
        bool empty() const;                                 //checks for emptiness
        void clear();                                       //clears the stack
        void push(const T& x);                              //add to top (copy)
        void push(T && x);                                  //add to top (move)
        void pop();                                         //remove from top
        T& top();                                           //return reference to top element (L-value)
        const T& top() const;                               //return reference to top elemtn (const reference)
        int size() const;                                   //return number of elements in the stack
        void print(std::ostream& os, char ofc = ' ') const;  //print stack to ostream os with seperator ofc
        const std::vector<T>& getData() const;

        
    
    private:
        std::vector<T> data;
    
    };
    
    
    
    template <typename T>
    std::ostream& operator<< (std::ostream& os, const Stack<T>& a);     //invoke print
    
    template <typename T>
    bool operator== (const Stack<T>&, const Stack <T>&);                 //returns true if stack elements are the same
    
    template <typename T>
    bool operator!= (const Stack<T>&, const Stack <T>&);                 //!=
    
    template <typename T>
    bool operator<= (const Stack<T>& a, const Stack <T>& b);            //
    
#include "stack.hpp"
    
    
}

#endif /* Stack_h */
