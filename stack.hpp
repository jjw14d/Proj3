

#ifndef Stack_hpp
#define Stack_hpp

template <typename T>
Stack<T>::Stack() {
    
}

template <typename T>
Stack<T>::~Stack(){
    
}

template <typename T>
Stack<T>::Stack(const Stack<T>& s2){
    data = s2.data;
}

template <typename T>
Stack<T>::Stack(Stack<T> && s2){
    data = std::move(s2.data);
}

template <typename T>
Stack<T>& Stack<T>::operator=(const Stack<T> & s2) {
    data = s2.data;
    return *this;
}

template <typename T>
Stack<T>& Stack<T>::operator=(Stack<T> && s2) {
    data = std::move(s2.data);
    return *this;
}

//*********************************************************

template <typename T>
bool Stack<T>::empty() const{
    return data.empty();
    
}

template <typename T>
void Stack<T>::clear(){
    data.clear();
}

template <typename T>
void Stack<T>::push(const T& x){
    data.push_back(x);
}

template <typename T>
void Stack<T>::push(T && x){
    data.push_back(std::move(x));
}

template <typename T>
void Stack<T>::pop(){
    data.pop_back();
}

template <typename T>
T& Stack<T>::top(){
    return data.back();
}

template <typename T>
const T& Stack<T>::top() const{
    return data.back();
}

template <typename T>
int Stack<T>::size() const{
    return data.size();
}

template <typename T>
void Stack<T>::print(std::ostream& os, char ofc)const{
    
    for (int i = 0; i < data.size(); i++) {
        os <<data[i];
        
        if ((i + 1) != data.size())
            os << ofc;
    }
}

//*******************************************************

template <typename T>
std::ostream& operator<< (std::ostream& os, const Stack<T>& a){
    
    a.print(os);
    return os;

}
    
template <typename T>
bool operator== (const Stack<T>& a, const Stack <T>& b){
   
    
    if (a.size() != b.size())
        return false;
    else{
        Stack<T> a2 = a;
        Stack<T> b2 = b;
        while (!a2.empty()){
            if (a2.top() != b2.top())
                return false;
            
            a2.pop();
            b2.pop();
        }
        return true;
    }
}

template <typename T>
bool operator!= (const Stack<T>& a, const Stack <T>& b){
    if (a == b)
        return false;
    else
        return true;
}

template <typename T>
bool operator<= (const Stack<T>& a, const Stack <T>& b){
    if (a.size() > b.size())
        return false;
    else{
        Stack<T> a2 = a;
        Stack<T> b2 = b;
        for (int i = 0; i < a.size(); i++) {
            if (a2.top() > b2.top())
                return false;
            a2.pop();
            b2.pop();
        }
        return true;
    }
}







#endif /* Stack_hpp */
